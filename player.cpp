#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
// This method initializes the player
// This is the constructor method
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    b = new Board();
    s = side;
    if (s == BLACK)
    {
        os = WHITE;
    }
    else
    {
        os = BLACK;
    }
    std::cerr << "constructor method" << std::endl;
    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
int Player::weightedMoves(Move m, Side side)
{
    int x = m.getX();
    int y = m.getY();
    // corners
    if((x == 0 || x == 7) && (y == 0 || y == 7))
    {
        return 6 + b->count(side);
    }
    // spots adjacent to edges
    else if (((x == 0 || x == 7) && (y == 1 || y == 6))
            || ((y == 0 || y == 7) && (x == 1 || x == 6)))
    {
        return 1 + b->count(side);
    }
    // edges
    else if ((x == 0 || x == 7) || (y == 0 || y == 7))
    {
        return 4 + b->count(side);
    }
    else
    {
        return 2 + b->count(side);
    }

}

Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    //std::vector<int> x_values;
    //std::vector<int> y_values;

    if (opponentsMove != NULL) 
    {
        b->doMove(opponentsMove, os);      
    }
    vector<Move> possible_moves = b-> listofmoves(s);
    // code to non-intelligently play othello
/*     Move *final_move = new Move(0,0);
    for (int i = 0; i < 8; i++)
    {

        for (int j = 0; j<8; j++)
        {
            Move temp_move = Move(i,j);
            if (b->checkMove(&temp_move, s))
            {
                b-> doMove(&temp_move, s);
                i = 9;
                j = 9;
                final_move->setX(temp_move.getX());
                final_move->setY(temp_move.getY());
            }
        }
    }*/


  if(!testingMinimax)
  {

        Move *final_move = new Move(0,0);
        int weight = -1;
        int counter = 0;
        for (unsigned int i = 0; i < possible_moves.size(); i++)
        {
   //         cerr << "possible_moves " << possible_moves[i].getX() << possible_moves[i].getY() << endl;
            if (weight < weightedMoves(possible_moves[i], s))
            {
                weight = weightedMoves(possible_moves[i], s);
                counter = i;
            }
        }
        final_move-> setX(possible_moves[counter].getX());
        final_move-> setY(possible_moves[counter].getY());
        b-> doMove(final_move, s);
        // cerr << "last move was " << final_move->getX() << final_move->getY() << endl;
        return final_move;
   }
    else
    {
        // PLY 1: create a parallel vector of boards for each move in vector of moves
        // then apply each move in the vector of moves to its corresponding board

        // create a vectors of available moves and future boards 
        vector<Move> moves_1; 
        vector<Board> boards_1;
        moves_1 = b->listofmoves(s); 

        Move *move_temp = new Move(0,0);

        for (unsigned int i = 0; i < boards_1.size(); i++)
        {
            boards_1.push_back(*b->copy()); // copied the board 

            move_temp-> setX(moves_1[i].getX());
            move_temp-> setY(moves_1[i].getY());
            boards_1[i].doMove(move_temp, s); // apply the corresponding move 
        } 

        // PLY 2: do the same for each board in the first vector of boards 
        vector<Move> moves_2; // permanently holds moves
        vector<Move> boardmoves_temp; 
        vector<Board> boards_2; 
        Move *movetemp = new Move(0,0);

        // iterate through ply 1 boards and create list of moves
        for(unsigned int i = 0; i < boards_1.size(); i++)
        {
            boardmoves_temp = boards_1[i].listofmoves(os); 
            // for each move in list of moves, push copy of board w/ applied move to board_2
            for(unsigned int j = 0; j < boardmoves_temp.size(); j++)
            {
                moves_2.push_back(boardmoves_temp[j]); // copy move to moves_2 vector
                Board temp = *boards_1[i].copy(); 
                
                movetemp-> setX(boardmoves_temp[j].getX());
                movetemp-> setY(boardmoves_temp[j].getY());
                temp.doMove(movetemp, os); 
                boards_2.push_back(temp); 
            }
        }

        // create parallel vector of scores
        vector<unsigned int> scores; 
        unsigned int highest_score = -64; 
        unsigned int temp; 
        Move *best_move = new Move(0,0);

        // 
        for(unsigned int i = 0; i < boards_2.size(); i++)
        {
            temp = boards_2[i].countBlack() + boards_2[i].countWhite() - boards_2[i].count(s); 
            if(temp > highest_score)
            {
                highest_score = temp; 
                best_move-> setX(moves_2[i].getX());
                best_move-> setY(moves_2[i].getY());
            }
        }

        b->doMove(best_move, s); 
        return best_move; 
    }




}
