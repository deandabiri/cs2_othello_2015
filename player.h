#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    Side os;
    Side s;
    Board* b;
 
    int weightedMoves(Move m, Side side);   
    Move *doMove(Move *opponentsMove, int msLeft);


    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
